﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.Serialization;
using System.IO;

namespace Dominio
{
    [Serializable]
    public class CTipoVehiculo : ISerializable
    {
        #region Atributos
        private static CTipoVehiculo instancia;
        private List<TipoVehiculo> tiposVehiculos = new List<TipoVehiculo>();
        #endregion

        #region Propiedades
        public static CTipoVehiculo Instancia
        {
            get
            {
                if(instancia == null)
                {
                    instancia = new CTipoVehiculo(); 
                }
                return instancia;
            }
        }
        #endregion

        #region Métodos
        /// <summary>
        /// Carga en la lista tiposVehiculos los tipso de vehiculos del archivo tiposVehiculos.txt
        /// </summary>
        public bool CargarTiposVehiculos(string ruta)
        {
            bool exito = false;

            if (File.Exists(ruta))
            {
                string[] lines = System.IO.File.ReadAllLines(ruta);
                if (lines != null)
                {
                    List<TipoVehiculo> tipoVehiculos = new List<TipoVehiculo>();

                    foreach (string line in lines)
                    {
                        List<string> marcaModelo = line.Split('@').ToList();
                        // Si la línea no contiene todos los datos necesarios, se saltea
                        // Limpiar
                        if (marcaModelo.Count != 3)
                        {
                            continue;
                        }
                        string marca = marcaModelo[0];
                        string modelo = marcaModelo[1];
                        double precioPorDia = 0;
                        double.TryParse(marcaModelo[2], out precioPorDia);
                        if(this.AltaTipoVehiculo(marca, modelo, precioPorDia))
                        {
                            exito = true;
                        }
                    }
                }
            }
            
            return exito;
        }

        public bool AltaTipoVehiculo(string marca, string modelo, double precioPorDia)
        {
            bool exito = false;

            TipoVehiculo tipo = BuscarTipoVehiculo(marca, modelo);

            if(tipo == null)
            {
                tiposVehiculos.Add(new TipoVehiculo(marca, modelo, precioPorDia));
                exito = true;
            }

            return exito;
        }
        
        /// <summary>
        /// Busca un vehiculo segun la marca y el modelo
        /// </summary>
        /// <param name="marca"></param>
        /// <param name="modelo"></param>
        /// <returns>Retorna el tipo de vehiculo, o null si no lo encuentra</returns>
        public TipoVehiculo BuscarTipoVehiculo(string marca, string modelo)
        {
            TipoVehiculo tipo = null;
            int contador = 0;

            while(tipo == null && contador < tiposVehiculos.Count)
            {
                if(tiposVehiculos[contador].Marca == marca && tiposVehiculos[contador].Modelo == modelo)
                {
                    tipo = tiposVehiculos[contador];
                }
                contador++;
            }
            return tipo;
        }

        private CTipoVehiculo() { }

        private CTipoVehiculo(SerializationInfo info, StreamingContext context)
        {
            this.tiposVehiculos = info.GetValue("listaTipos", typeof(List<TipoVehiculo>)) as List<TipoVehiculo>;
            CTipoVehiculo.instancia = this;
        }

        public void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            info.AddValue("listaTipos", this.tiposVehiculos, typeof(List<TipoVehiculo>));
        }
        #endregion

    }
}
