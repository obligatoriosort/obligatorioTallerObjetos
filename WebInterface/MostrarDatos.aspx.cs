﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Dominio;


namespace WebInterface
{
    public partial class MostrarDatos : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            GridView1.DataSource = CAlquiler.Instancia.ListaAlquileres;
            GridView1.DataBind();

            GridView2.DataSource = CCliente.Instancia.ListaClientes;
            GridView2.DataBind();

            GridView3.DataSource = CVehiculo.Instancia.ListaVehiculos;
            GridView3.DataBind();
        }
    }
}