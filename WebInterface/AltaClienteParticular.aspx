﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Master.Master" AutoEventWireup="true" CodeBehind="AltaClienteParticular.aspx.cs" Inherits="WebInterface.AltaClienteParticular" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:Label ID="Label1" runat="server" Text="Alta Cliente Particular"></asp:Label>
    <br />
    <br />
    <asp:Label ID="Label2" runat="server" Text="Año de registro"></asp:Label>
    <asp:TextBox ID="txtAnio" runat="server" TextMode="Date"></asp:TextBox>
    <br />
    <asp:Label ID="Label3" runat="server" Text="Telefono"></asp:Label>
    <asp:TextBox ID="txtTel" runat="server" OnTextChanged="TextBox1_TextChanged"></asp:TextBox>
    <asp:RequiredFieldValidator ID="RequiredFieldValidator9" runat="server" ControlToValidate="txtTel" ErrorMessage="Ingrese Telefono">*Error</asp:RequiredFieldValidator>
    <br />
    <asp:Label ID="Label" runat="server" Text="Nombre"></asp:Label>
    <asp:TextBox ID="txtNombre" runat="server" OnTextChanged="TextBox1_TextChanged1"></asp:TextBox>
    <asp:RequiredFieldValidator ID="RequiredFieldValidator8" runat="server" ControlToValidate="txtNombre" ErrorMessage="Ingrese Nombre">*Error</asp:RequiredFieldValidator>
    <br />
    <asp:Label ID="Label4" runat="server" Text="Apellido"></asp:Label>
    <asp:TextBox ID="txtApellido" runat="server" Height="22px" Width="128px"></asp:TextBox>
    <asp:RequiredFieldValidator ID="RequiredFieldValidator7" runat="server" ControlToValidate="txtApellido" ErrorMessage="Ingrese Apellido">*Error</asp:RequiredFieldValidator>
    <br />
    <asp:Label ID="Label7" runat="server" Text="Documento"></asp:Label>
    <asp:TextBox ID="txtDoc" runat="server"></asp:TextBox>
    <asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" ControlToValidate="txtDoc" ErrorMessage="Ingrese Documento">*Error</asp:RequiredFieldValidator>
    <br />
    <asp:Label ID="Label5" runat="server" Text="Pais Doc"></asp:Label>
    <asp:TextBox ID="txtPaisDoc" runat="server"></asp:TextBox>
    <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ControlToValidate="txtPaisDoc" ErrorMessage="Ingrese pais Documento">*Error</asp:RequiredFieldValidator>
    <br />
    <asp:Label ID="Label6" runat="server" Text="Tipo Doc"></asp:Label>
    <asp:DropDownList ID="ddlTipoDoc" runat="server">
    </asp:DropDownList>
    <br />
    <br />
    <asp:ValidationSummary ID="ValidationSummary1" runat="server" />
    <asp:Label ID="lblError" runat="server"></asp:Label>
    <br />
    <asp:Button ID="btnCrear" runat="server" OnClick="btnCrear_Click" Text="Crear" />
</asp:Content>
