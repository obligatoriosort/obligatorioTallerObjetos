﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.Serialization;
using System.IO;


namespace Dominio
{
    [Serializable]
    public class CAlquiler : ISerializable
    {
        #region Atributos
        private List<Alquiler> alquileres = new List<Alquiler>();
        private static CAlquiler instancia;
        #endregion

        #region Propiedades
        public static CAlquiler Instancia
        {
            get
            {
                if(instancia == null)
                {
                    instancia = new CAlquiler();
                }
                return instancia;
            }
        }

        public List<Alquiler> ListaAlquileres
        {
            get
            {
                return this.alquileres;
            }
        }

        #endregion

        #region Métodos
        /// <summary>
        /// Registro de alquileres
        /// </summary>
        /// <param name="vehiculo">Vehiculo que se alquila</param>
        /// <param name="cliente">Cliente que alquila</param>
        /// <param name="fechaInicio">Fecha de comienzo de alquiler</param>
        /// <param name="fechaFinal">Fecha de finalización de alquiler</param>
        /// <returns></returns>
        public bool AltaAlquiler(Vehiculo vehiculo, Cliente cliente, DateTime fechaInicio, DateTime fechaFinal)
        {
            bool exito = false;

            // Chequeamos que el alquiler del vehiculo no se haya realizado por otro mientras se gestionaba este.
            if (MatriculaDisponible(vehiculo.Matricula, fechaInicio, fechaFinal))
            {
                this.alquileres.Add(new Alquiler(cliente, vehiculo, fechaInicio, fechaFinal));

                exito = true;
            }

            return exito;
        }

        /// <summary>
        /// Busca que el vehiculo haya sido alquilado y lo devuelve luego de llamar Devolución en su alquiler
        /// </summary>
        /// <param name="matricula">Matricula de vehiculo alquilado</param>
        /// <returns>Objeto alquiler</returns>
        public Alquiler DevolverVehiculo(string matricula)
        {
            Alquiler alq = BuscarAlquiler(matricula);

            if (alq != null)
            {
                alq.Devolucion();
            }


            return alq;
        }

        /// <summary>
        /// Retorna una lista de alquileres que no se hayan devuelto al día de la fecha y que deberían de haber sido devueltos
        /// </summary>
        /// <returns>Lista de alquileres atrasados</returns>
        public List<Alquiler> ListarAtrasados()
        {
            List<Alquiler> atrasados = new List<Alquiler>();

            foreach(Alquiler alq in alquileres)
            {
                if (alq.FechaFinal < DateTime.Today && !alq.Finalizado)
                {
                    atrasados.Add(alq);
                }
            }
            atrasados.Sort();

            return atrasados;
        }

        /// <summary>
        ///  Crea un log de texto o agrega una linea si ya existe, con el nombre del emisor y fecha expedido
        /// </summary>
        /// <param name="nombre"></param>
        /// <returns></returns>
        public void ListaEmisoresAtrasados(string nombre, string ruta)
        {
            DateTime fecha = DateTime.Now;
            string cadena = fecha.ToString() + "-" + nombre;
            
            if (System.IO.File.Exists(ruta))
            {
                using (System.IO.StreamWriter file = new System.IO.StreamWriter(ruta, true))
                {
                    file.WriteLine(cadena); //se agrega información al documento
                    file.Close();
                }
            }
            else
            {
                // crear el fichero
                using (FileStream fileStream = System.IO.File.Create(ruta))
                {
                    fileStream.Close();

                    StreamWriter sw = new StreamWriter(ruta, true);
                    sw.WriteLine(cadena);
                    sw.Close();
                }
            }

        }

        /// <summary>
        /// Busca en la lista de alquileres matriculas de autos que estén alquilados entre fechaInicio y fechaFinal
        /// </summary>
        /// <param name="fechaInicio"></param>
        /// <param name="fechaFinal"></param>
        /// <returns>Retorna matriculas no disponibles entre dos fechas</returns>
        public List<string> MatriculasNoDisponibles(DateTime fechaInicio, DateTime fechaFinal)
        {
            List<string> matriculas = new List<string>();

            foreach(Alquiler alq in alquileres)
            {
                if(!alq.Finalizado && ((fechaInicio >= alq.FechaInicio && fechaInicio <= alq.FechaFinal) 
                    || (fechaFinal >= alq.FechaInicio && fechaFinal <= alq.FechaFinal)) )
                {
                    matriculas.Add(alq.Matricula);
                }
            }
            return matriculas;
        }

        public bool MatriculaDisponible(string matricula, DateTime fechaInicio, DateTime fechaFinal)
        {
            bool exito = true;

            List<string> matriculas = MatriculasNoDisponibles(fechaInicio, fechaFinal);

            if (matriculas.Contains(matricula))
            {
                exito = false;
            }

            return exito;
        }

        /// <summary>
        /// Busca un objeto alquiler por la matricula del vehiculo alquilado
        /// </summary>
        /// <param name="matricula">Matricula de vehiculo alquilado</param>
        /// <returns>Objeto alquiler</returns>
        public Alquiler BuscarAlquiler(string matricula)
        {
            List<Alquiler> alquileresDeMatricula = new List<Alquiler>();
            DateTime alquilerMasTemprano = DateTime.MaxValue;

            foreach (Alquiler alq in alquileres)
            {
                if(!alq.Finalizado && alq.Matricula.Equals(matricula))
                {
                    alquileresDeMatricula.Add(alq);
                    // Además de agregar el alquiler a la lista de alquileres de ese auto, buscamos la fecha del alquiler "actual"
                    // o en otras palabras, cuál es el alquiler que esta en marcha
                    if (alq.FechaInicio < alquilerMasTemprano)
                    {
                        alquilerMasTemprano = alq.FechaInicio;
                    }
                }
            }

            // Con la fecha del alquiler actual y la lista de alquileres de matricula, buscamos cual es el alquiler que nos interesa
            int i = 0;
            Alquiler alquiler = null;

            while(alquiler == null && i < alquileresDeMatricula.Count)
            {
                if(alquileresDeMatricula[i].FechaInicio == alquilerMasTemprano)
                {
                    alquiler = alquileresDeMatricula[i];
                }
                i++;
            }

            return alquiler;
        }


        // Manejo de serialización
        private CAlquiler() { }

        public void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            info.AddValue("listaAlq", this.alquileres, typeof(List<Alquiler>));
        }

        private CAlquiler(SerializationInfo info, StreamingContext context)
        {
            this.alquileres = info.GetValue("listaAlq", typeof(List<Alquiler>)) as List<Alquiler>;
            CAlquiler.instancia = this;
        }

        #endregion


    }
}
