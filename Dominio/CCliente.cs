﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.Serialization;

namespace Dominio
{
    [Serializable]
    public class CCliente : ISerializable
    {
        #region Atributos
        private List<Cliente> clientes = new List<Cliente>();
        private Enum enumTipoDoc;
        private static CCliente instancia;
        #endregion

        #region Propiedades
        public static CCliente Instancia
        {
            get
            {
                if(instancia == null)
                {
                    instancia = new CCliente();
                }
                return instancia;
            }
        }

        public Enum EnumTipoDoc
        {
            get
            {
                return enumTipoDoc;
            }

            set
            {
                enumTipoDoc = value;
            }
        }

        public List<Cliente> ListaClientes
        {
            get
            {
                return this.clientes;
            }
        }
        #endregion

        #region Métodos
        /// <summary>
        /// Registro de nuevos clientes particulares
        /// </summary>
        /// <param name="id">Identificador, CI, DNI, Pasaporte</param>
        /// <param name="anioRegistro">Año en el que se registro el cliente</param>
        /// <param name="nombre">Nombre del cliente</param>
        /// <param name="telefono">Telefono del cliente</param>
        /// <param name="apellido">Apellido del cliente</param>
        /// <param name="tipoDoc">Tipo de documente, CI, DNI, Pasaporte</param>
        /// <param name="paisDoc">País en el que se emite el documeento</param>
        /// <returns>Verdadero si el cliente se registro exitosamente</returns>
        public bool AltaParticular(string id, DateTime anioRegistro, string nombre, string telefono, string apellido,
            string tipoDoc, string paisDoc)
        {
            bool exito = false;
            Cliente cliente = BuscarCliente(id);

            

            if (cliente == null && ValidarDoc(tipoDoc))
            {
                clientes.Add(new Particular(id, anioRegistro, telefono, nombre, apellido, CrearEnumTipoDoc(tipoDoc), paisDoc));
                exito = true;
            }
            return exito;
        }

        /// <summary>
        /// Registro de nuevos clientes empresa
        /// </summary>
        /// <param name="id">RUT de empresa</param>
        /// <param name="anioRegistro">Año en el que se registro el cliente</param>
        /// <param name="nombre">Nombre de contacto</param>
        /// <param name="telefono">Telefono de contacto</param>
        /// <param name="razonSocial">Razón social de la empresa</param>
        /// <returns>Verdadero si el cliente se registro exitosamente</returns>
        public bool AltaEmpresa(string id, DateTime anioRegistro, string nombre, string telefono, string razonSocial)
        {
            bool exito = false;
            Cliente cliente = BuscarCliente(id);

            if (cliente == null)
            {
                clientes.Add(new Empresa(id, anioRegistro, telefono, nombre, razonSocial));
                exito = true;
            }
            return exito;
        }

        /// <summary>
        /// Busca el cliente particular/empresa
        /// </summary>
        /// <param name="id">Identificador del cliente, RUT, CI, DNI, Pasaporte</param>
        /// <returns>Retorna el objeto cliente encontrado o null</returns>
        public Cliente BuscarCliente(string id)
        {
            int i = 0;
            bool existe = false;
            Cliente cliente = null;
            while (i < clientes.Count && !existe)
            {
                if (clientes[i].Identificador() == id)
                {
                    cliente = clientes[i];
                    existe = true;
                }
                i++;
            }
            return cliente;
        }
        #endregion

        #region Metodos para manejo de Enums
        /// <summary>
        /// Válida que la cadena ingresada sea un tipoDoc posible
        /// </summary>
        /// <param name="turno">tipoDoc</param>
        /// <returns>bool</returns>
        public bool ValidarDoc(string tipoDoc)
        {
            bool exito = false;

            string[] turnos = Enum.GetNames(typeof(EnumTipoDoc));

            if (turnos.Contains(tipoDoc))
            {
                exito = true;
            }
            return exito;
        }

        public EnumTipoDoc CrearEnumTipoDoc(string tipoDoc)
        {
            EnumTipoDoc enumTipoDoc = (EnumTipoDoc)Enum.Parse(typeof(EnumTipoDoc), tipoDoc);
            return enumTipoDoc;
        }

        /// <summary>
        /// Devuelve una lista con los tipoDoc posibles
        /// </summary>
        /// <returns>Lista de tipoDocs</returns>
        public List<string> MostrarTipoDocumentos()
        {
            string[] tipoDocs = Enum.GetNames(typeof(EnumTipoDoc));
            List<string> resultado = new List<string>();

            for (int i = 0; i < tipoDocs.Length; i++)
            {
                resultado.Add(tipoDocs[i]);
            }
            return resultado;
        }

        private CCliente() { }

        private CCliente(SerializationInfo info, StreamingContext context)
        {
            this.clientes = info.GetValue("listaCliente", typeof(List<Cliente>)) as List<Cliente>;
            CCliente.instancia = this;
        }

        public void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            info.AddValue("listaCliente", this.clientes, typeof(List<Cliente>));
        }
        #endregion
    }
}
