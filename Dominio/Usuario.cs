﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dominio
{
    [Serializable]
    public class Usuario
    {
        #region Atributos
        private string nombre;
        private string password;
        private EnumRol enumRol;
        #endregion

        #region Propiedades
        public string Nombre
        {
            get
            {
                return nombre;
            }
            set
            {
                nombre = value;
            }
        }

        public string Password
        {
            get
            {
                return this.password;
            }
        }

        public EnumRol EnumRol
        {
            get
            {
                return enumRol;
            }

            set
            {
                enumRol = value;
            }
        }
        #endregion

        public Usuario(string nombre, string password, EnumRol enumRol)
        {
            this.nombre = nombre;
            this.password = password;
            this.enumRol = enumRol;
        }

    }
}
