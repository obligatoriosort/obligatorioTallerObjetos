﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dominio
{
    [Serializable]
    public enum EnumTipoDoc
    {
        CI = 0,
        DNI = 1,
        pasaporte = 2
    }
}
