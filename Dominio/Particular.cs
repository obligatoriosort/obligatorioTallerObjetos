﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.Serialization;

namespace Dominio
{
    [Serializable]
    public class Particular : Cliente
    {
        #region Atributo
        private static int descuento = 2;
        private string documento;
        private string nombre;
        private string apellido;
        private EnumTipoDoc tipoDoc;
        private string paisDoc;
        #endregion

        #region Metodos
        public Particular(string documento, DateTime anioRegistro, string telefono, string nombre, string apellido,
            EnumTipoDoc tipoDoc, string paisDoc) : base(anioRegistro, telefono)
        {
            this.documento = documento;
            this.nombre = nombre;
            this.apellido = apellido;
            this.tipoDoc = tipoDoc;
            this.paisDoc = paisDoc;
        }

        /// <summary>
        /// Retorna un descuento en caso de que el cliente califique para ello
        /// </summary>
        /// <returns>Retorna el descuento, o 0</returns>
        public override int Descuento()
        {
            int desc = 0;
            if (this.esUruguay())
            {
                desc =  Particular.descuento;
            }
            return desc;
        }

        public override string Identificador()
        {
            return this.documento;
        }

        /// <summary>
        /// Retorna si el cliente es uruguayo o no
        /// </summary>
        /// <returns>Verdadero si es uruguayo</returns>
        private bool esUruguay()
        {
            bool esUru = false;
            if (this.paisDoc.ToLower() == "uruguay")
            {
                esUru = true;
            }
            return esUru;
        }

        public override string ToString()
        {
            string datos = String.Format("Documento: {0}, Fecha Registro: {1}, Telefono: {2}, Nombre Completo: {3} {4}, Tipo Documento: {5}, País Documento {6}",
                this.documento, base.AnioRegistro, base.Telefono, this.nombre, this.apellido, this.tipoDoc.ToString(), this.paisDoc);

            return datos;
        }
        #endregion
    }
}
