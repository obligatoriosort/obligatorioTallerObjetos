﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.Serialization;

namespace Dominio
{
    [Serializable]
    public class Empresa : Cliente
    {
        #region Atributos
        private string nombreContacto;
        private string razonSocial;
        private string rut;
        #endregion

        public Empresa(string rut, DateTime anioRegistro, string telefono, string nombreContacto, string razonSocial)
            : base(anioRegistro, telefono)
        {
            this.rut = rut;
            this.nombreContacto = nombreContacto;
            this.razonSocial = razonSocial;
        }

        public override int Descuento()
        {
            int desc;
            DateTime registro = this.AnioRegistro;
            DateTime hoy = DateTime.Today;
            int anios = 0;

            if(registro.Year < hoy.Year)
            {
                anios = hoy.Year - registro.Year;
                if(registro.Month >= hoy.Month && registro.Day > hoy.Day)
                {
                    anios--;
                }
            }

            desc = anios * 2;
            if (desc > 20)
            {
                desc = 20;
            }
            return desc;
        }

        public override string Identificador()
        {
            return this.rut;
        }

        public override string ToString()
        {
            string datos = String.Format("RUT: {0}, Fecha Registro: {1}, Telefono: {2}, Nombre Contacto: {3}, Razon Social: {4}",
                this.rut, base.AnioRegistro, base.Telefono, this.nombreContacto, this.razonSocial);

            return datos;
        }
    }
}
