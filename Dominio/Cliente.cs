﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.Serialization;

namespace Dominio
{
    // se hace Iserializable, porque el serialize va a llamar objetos de tipo cliente, y particular es hijo de cliente
    [Serializable]
    public abstract class Cliente
    {
        #region Atributos
        private DateTime anioRegistro;
        private string telefono;
        #endregion

        #region Propiedades
        public DateTime AnioRegistro
        {
            get
            {
                return anioRegistro;
            }
            set
            {
                anioRegistro = value;
            }
        }

        public string Telefono
        {
            get
            {
                return this.telefono;
            }

            set
            {
                this.telefono = value;
            }
        }

        public string Id
        {
            get
            {
                return this.Identificador();
            }
        }
        #endregion

        public Cliente(DateTime anioRegistro, string telefono)
        {
            this.anioRegistro = anioRegistro;
            this.telefono = telefono;
        }

        public abstract int Descuento();

        public abstract string Identificador();
    }
}
