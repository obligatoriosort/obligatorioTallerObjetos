﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Dominio;

namespace WebInterface
{
    public partial class AltaCliente : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["rol"] == null)
            {
                Response.Redirect("Login.aspx");
            }

            if (!IsPostBack)
            {
                ddlTipoDoc.DataSource = Fachada.TipoDeDocumentosPosibles();
                ddlTipoDoc.DataBind();
            }
        }

        protected void BtnEmpresa_Click(object sender, EventArgs e)
        {
            PnlEmpresa.Visible = true;
            BtnEmpresa.Visible = false;
            BtnParticular.Visible = false;
        }

        protected void BtnParticular_Click(object sender, EventArgs e)
        {
            PnlParticular.Visible = true;
            BtnEmpresa.Visible = false;
            BtnParticular.Visible = false;
        }

        protected void BtnCrearEmpresa_Click(object sender, EventArgs e)
        {
            DateTime fechaRegistro = DateTime.MinValue;
            DateTime.TryParse(txtAnio.Text, out fechaRegistro);
            string telefono = txtTel.Text;
            string nombre = txtNombreContacto.Text;
            string razonSocial = txtRazonSocial.Text;
            string rut = txtRut.Text;


            if (Fachada.AltaEmpresa(rut, telefono, nombre, razonSocial, fechaRegistro))
            {

                lblError.Text = "La empresa se dio de alta correctamente";
                if (Session["usuarioAlquiler"] != null && (bool)Session["usuarioAlquiler"])
                {
                    Session["cliente"] = rut;
                    Response.Redirect("AltaAlquiler.aspx");
                }

            }
            else
            {
                lblError.Text = "Verifique los datos";
            }
        }

        protected void BtnCrearParticular_Click(object sender, EventArgs e)
        {
            DateTime fechaRegistro = DateTime.MinValue;
            DateTime.TryParse(txtAnio.Text, out fechaRegistro);
            string telefono = TxtTelCliente.Text;
            string nombre = txtNombre.Text;
            string apellido = txtApellido.Text;
            string paisdoc = txtPaisDoc.Text;
            string doc = txtDoc.Text;

            if (Fachada.AltaParticular(doc, ddlTipoDoc.SelectedValue, paisdoc, telefono, nombre, apellido, fechaRegistro))
            {
                lblError.Text = "El cliente se dio de alta correctamente";
                if(Session["usuarioAlquiler"] != null && (bool)Session["usuarioAlquiler"])
                {
                    Session["cliente"] = doc;
                    Response.Redirect("AltaAlquiler.aspx");
                }
                else
                {
                    lblError.Visible = true;
                }
            }
            else
            {
                lblError.Visible = true;
                lblError.Text = "Verifique los datos";
            }
        }
    }
}