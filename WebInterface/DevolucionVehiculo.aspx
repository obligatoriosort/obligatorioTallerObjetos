﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Master.Master" AutoEventWireup="true" CodeBehind="DevolucionVehiculo.aspx.cs" Inherits="WebInterface.DevolucionVehiculo" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script src="scripts/DevolucionVehiculo.js"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:Label ID="Label1" runat="server" Text="Devolucion de vehiculo"></asp:Label>
    <br />
    <br />
    <asp:Label ID="Label2" runat="server" Text="Ingrese matricula"></asp:Label>
    <asp:TextBox ID="txtBoxMatricula" runat="server" ClientIDMode="Static"></asp:TextBox>
    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtBoxMatricula" ErrorMessage="Matricula es un campo requerido">*Error</asp:RequiredFieldValidator>
    <br />
    <asp:Label ID="LblInfo" runat="server" Text="No hay devoluciones pendientes de esta matricula" Visible="False"></asp:Label>
    <br />
    <asp:Button ID="btnDevolver" runat="server" OnClick="btnDevolver_Click" Text="Devolver" OnClientClick="return validarVacio()" />
    <br />
    <br />
    <asp:GridView ID="GridAlquiler" runat="server" AutoGenerateColumns="False">
        <Columns>
            <asp:BoundField AccessibleHeaderText="Fecha de Inicio" DataField="FechaInicio" HeaderText="Fecha de Inicio" />
            <asp:BoundField AccessibleHeaderText="Fecha Final" DataField="FechaFinal" HeaderText="Fecha Final" />
            <asp:BoundField AccessibleHeaderText="Costo" DataField="Monto" HeaderText="Costo" />
            <asp:BoundField AccessibleHeaderText="Recargo" DataField="Recargo" HeaderText="Recargo" />
            <asp:BoundField AccessibleHeaderText="Datos de cliente" DataField="Cliente" HeaderText="Datos de cliente" />
            <asp:BoundField AccessibleHeaderText="Datos de Vehiculo" DataField="Vehiculo" HeaderText="Datos de Vehiculo" />
        </Columns>
    </asp:GridView>
    <br />
    <br />
    </asp:Content>
