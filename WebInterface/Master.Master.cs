﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WebInterface
{
    public partial class Master : System.Web.UI.MasterPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["rol"] != null)
            {
                if (Session["rol"].ToString() == "vendedor")
                {
                    MenuVendedor.Visible = true;
                    MenuAdmin.Visible = false;
                }
                else if (Session["rol"].ToString() == "administrador")
                {
                    MenuAdmin.Visible = true;
                    MenuVendedor.Visible = false;
                }
            }
            else
            {
                MenuAdmin.Visible = false;
                MenuVendedor.Visible = false;
            }
        }
        protected void Menu1_MenuItemClick(object sender, MenuEventArgs e)
        {

        }
    }
}