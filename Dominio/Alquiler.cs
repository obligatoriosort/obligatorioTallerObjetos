﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dominio
{
    [Serializable]
    public class Alquiler : IComparable<Alquiler>
    {
        #region Atributos
        private Cliente cliente;
        private Vehiculo vehiculo;
        private DateTime fechaInicio;
        private DateTime fechaFinal;
        private double monto;
        private bool finalizado = false;
        // Se podría hacer que la property de recargo calcule cada vez que la pedís el costo del recargo. Siempre da 0 si la fecha de entrega es en el futuro
        private double recargo = 0;
        #endregion

        #region Propiedades
        public DateTime FechaFinal
        {
            get
            {
                return this.fechaFinal;
            }
        }

        public DateTime FechaInicio
        {
            get
            {
                return this.fechaInicio;
            }
        }

        public Vehiculo Vehiculo
        {
            get
            {
                return this.vehiculo;
            }
        }

        public string Matricula
        {
            get
            {
                return this.vehiculo.Matricula;
            }
        }

        public double Monto
        {
            get
            {
                return this.monto;
            }
        }

        public Cliente Cliente
        {
            get
            {
                return this.cliente;
            }
        }

        public string DatosCliente
        {
            get
            {
                return this.cliente.ToString();
            }
        }

        public double Recargo
        {
            get
            {
                return this.recargo;
            }
            set
            {
                this.recargo = value;
            }
        }

        public bool Finalizado
        {
            get
            {
                return this.finalizado;
            }
        }
        #endregion

        #region Métodos

        public Alquiler(Cliente cli, Vehiculo vehiculo, DateTime fechaInicio, DateTime fechaFinal)
        {
            this.cliente = cli;
            this.vehiculo = vehiculo;
            this.fechaInicio = fechaInicio;
            this.fechaFinal = fechaFinal;
            this.monto = this.CalcularCosto();
        }

        /// <summary>
        /// Calcula el costo del alquiler tomando en cuenta los días entre fechaInicial y fechaFinal, el precioPorDia del tipo de vehiculo
        /// y el descuento del cliente
        /// </summary>
        /// <returns>Devuelve el costo del alquiler</returns>
        public double CalcularCosto()
        {
            double costo = 0;

            int diasAlquilado = (this.fechaFinal - this.fechaInicio).Days + 1;

            double costoBase = diasAlquilado * this.vehiculo.PrecioPorDia;
            double descuento = costoBase * this.cliente.Descuento() / 100;
            costo = costoBase - descuento;

            return costo;
        }

        /// <summary>
        /// Calcula el costo atrasado del alquiler tomando en cuenta los días entre fechaInicial y fechaFinal, el precioPorDia del tipo de vehiculo
        /// y el descuento del cliente
        /// </summary>
        /// <param name="fechaEntrega">Fecha en la que actualmente se devuelve el vehiculo</param>
        /// <returns>Retorna 0 si el vehiculo se entrego en fecha o antes, o algo mayor si se entrego tarde</returns>
        private double CalcularCosto(DateTime fechaEntrega)
        {
            double costo = 0;

            double diasAlquilado = (fechaEntrega - this.fechaFinal).Days + 1;

            double costoBase = diasAlquilado * this.vehiculo.PrecioPorDia;
            double descuento = costoBase * this.cliente.Descuento() / 100;
            costo = costoBase - descuento;

            return costo;
        }

        public int CompareTo(Alquiler obj)
        {
            // Ordenar alquileres por fecha de devolución descendente, y matricula ascendente
            int res = obj.fechaFinal.CompareTo(this.fechaFinal);
            if (res == 0)
            {
                res = this.vehiculo.Matricula.CompareTo(obj.vehiculo.Matricula);
            }
            return res;
        }

        /// <summary>
        /// Modifica el estado del alquiler para dar cuenta de la devolución, y calcula un recargo en caso de ser necesario 
        /// </summary>
        public void Devolucion()
        {
            // Nos dijamos primero si esta atrasada la devolución, ya que el calculo es menos complejo.
            if (DateTime.Today > this.fechaFinal)
            {
                this.recargo = this.CalcularCosto(DateTime.Today);
            }
            this.finalizado = true;
        }

        #endregion
    }
}
