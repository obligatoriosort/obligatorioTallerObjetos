﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Dominio;
using System.Configuration;
using System.Data;

namespace WebInterface
{
    public partial class AltaAlquiler : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

            if (Session["rol"] == null)
            {
                Response.Redirect("Login.aspx");
            }

            if (Session["usuarioAlquiler"] != null && (bool)Session["usuarioAlquiler"])
            {
                // me parece que sobra este control
                if (!IsPostBack)
                {
                    if(Session["cliente"] != null) // AltaCliente mete en Session["cliente"] al cliente recien creado
                    {
                        // chequear bien que hace el post back
                        TextBoxIdCliente.Text = Session["cliente"].ToString();
                        Session.Remove("usuarioAlquiler");
                        Session.Remove("cliente");
                    }
                }
            }

        }

        protected void BtnBuscCliente_Click(object sender, EventArgs e)
        {
            string id = TextBoxIdCliente.Text;

            Cliente cli = Fachada.BuscarCliente(id); // Mejorar esto

            if(cli != null)
            {
                // Mejorar esto con las boxes (vichar obligatorio P2)
                PnlFecha.Visible = true;
                PnlCliente.Visible = false;
            }
            else
            {
                Session["usuarioAlquiler"] = true; // Esta variable sirve para que en AltaCliente, al confirmar el registro te devuelva a AltaAlquiler
                Response.Redirect("AltaCliente.aspx"); // Tiene que ser una sola página
            }
        }

        protected void BtnBuscarVehiculo_Click(object sender, EventArgs e)
        {
            DateTime fechaEntrega = DateTime.MinValue;

            DateTime.TryParse(TxtBoxFechaEntrega.Text, out fechaEntrega);

            if (!fechaEntrega.Equals(DateTime.MinValue) && fechaEntrega > DateTime.Today)
            {
                ListBoxMarcas.DataSource = Fachada.MarcasDisponibles(DateTime.Now, fechaEntrega);
                ListBoxMarcas.DataBind();
                PnlMarca.Visible = true;
                LblErrorFecha.Visible = false;
                PnlFecha.Visible = false;
            }
            else
            {
                LblErrorFecha.Visible = true;
            }
        }

        protected void ListBoxMarcas_SelectedIndexChanged(object sender, EventArgs e)
        {
            string marca = ListBoxMarcas.SelectedValue;

            DateTime fechaEntrega = DateTime.MinValue;
            DateTime fechaInicio = DateTime.MinValue;

            DateTime.TryParse(TxtBoxFechaInicio.Text, out fechaInicio);
            DateTime.TryParse(TxtBoxFechaEntrega.Text, out fechaEntrega);

            if(!fechaEntrega.Equals(DateTime.MinValue) && !fechaEntrega.Equals(DateTime.MinValue))
            {
                ListBoxModelos.DataSource = Fachada.ModelosDisponibles(fechaInicio, fechaEntrega, marca);
                ListBoxModelos.DataBind();

                PnlModelo.Visible = true;
                PnlMarca.Visible = false;
            }
        }

        protected void ListBoxModelos_SelectedIndexChanged(object sender, EventArgs e)
        {
            string marca = ListBoxMarcas.SelectedValue;
            string modelo = ListBoxModelos.SelectedValue;

            DateTime fechaEntrega = DateTime.MinValue;
            DateTime fechaInicio = DateTime.MinValue;

            DateTime.TryParse(TxtBoxFechaInicio.Text, out fechaInicio);
            DateTime.TryParse(TxtBoxFechaEntrega.Text, out fechaEntrega);


            Repeater1.DataSource = Fachada.VehiculosAlquilables(fechaInicio, fechaEntrega, marca, modelo);
            Repeater1.DataBind();;

            PnlVehiculo.Visible = true;
            PnlModelo.Visible = false;
            PnlAlquilado.Visible = true;
        }

        protected void Repeater2_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                Repeater repeater2 = (Repeater)e.Item.FindControl("Repeater2");
                Vehiculo veh = (Vehiculo)e.Item.DataItem;

                if (veh.Fotos.Count > 0)
                {
                    List<string> fotosConPath = new List<string>();
                    foreach(string foto in veh.Fotos)
                    {
                        fotosConPath.Add(ConfigurationManager.AppSettings["rutaFotos"] + foto);
                    }
                    repeater2.DataSource = fotosConPath;
                    repeater2.DataBind();
                }
                else
                {
                    repeater2.Visible = false;
                }
            }
        }

        protected void BtnAlquilar(object sender, EventArgs e)
        {
            RepeaterItem repItem = (sender as Button).Parent as RepeaterItem;
            HiddenField hf = repItem.FindControl("HfMatricula") as HiddenField;
            string matricula = hf.Value;

            DateTime fechaEntrega = DateTime.MinValue;
            DateTime fechaInicio = DateTime.MinValue;

            DateTime.TryParse(TxtBoxFechaInicio.Text, out fechaInicio);
            DateTime.TryParse(TxtBoxFechaEntrega.Text, out fechaEntrega);

            fechaEntrega = fechaEntrega.Add(TimeSpan.Parse(TxtBoxHoraEntrega.Text));
            fechaInicio = fechaInicio.Add(TimeSpan.Parse(TxtBoxHoraInicio.Text));

            if (Fachada.AltaAlquiler(TextBoxIdCliente.Text, matricula, fechaInicio, fechaEntrega))
            {
                LblAlquilado.Visible = true;
                PnlAlquilado.Visible = true;
                PnlVehiculo.Visible = false;

                List<Alquiler> alq = new List<Alquiler>();
                alq.Add(Fachada.buscarAlquiler(matricula));
                GrillaAlquilados.DataSource = alq;
                GrillaAlquilados.DataBind();
            }
            else
            {
                LblAlquilado.Visible = true;
                LblAlquilado.Text = "El alquiler no se pudo realizar";
            }
        }
    }
}
