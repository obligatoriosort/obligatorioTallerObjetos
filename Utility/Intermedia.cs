﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.IO;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using Dominio;

namespace Utility
{
    [Serializable]
    public class Intermedia
    {
        #region Atributos
        private CCliente cCliente;
        private CAlquiler cAlquiler;
        private CVehiculo cVehiculo;
        private CUsuario cUsuario;
        private CTipoVehiculo cTipoVehiculo;
        private string rutaSerial;
        #endregion

        #region Propiedades
        #endregion

        #region Métodos
        public Intermedia(string ruta)
        {
            this.rutaSerial = ruta;
            this.cCliente = CCliente.Instancia;
            this.cAlquiler = CAlquiler.Instancia;
            this.cVehiculo = CVehiculo.Instancia;
            this.cUsuario = CUsuario.Instancia;
            this.cTipoVehiculo = CTipoVehiculo.Instancia;
        }

        public void Serializar()
        {
            BinaryFormatter bf = new BinaryFormatter();
            FileStream fs = new FileStream(this.rutaSerial, FileMode.Create);
            bf.Serialize(fs, this);
            fs.Close();
        }

        public void DesSerializar()
        {
            BinaryFormatter bf = new BinaryFormatter();
            FileStream fs = new FileStream(this.rutaSerial, FileMode.Open);
            Intermedia rep = bf.Deserialize(fs) as Intermedia;
            fs.Close();
            //NO lo usamos para nada
        }
        #endregion
    }
}