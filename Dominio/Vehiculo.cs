﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dominio
{
    [Serializable]
    public class Vehiculo
    {
        #region Atributos
        private TipoVehiculo tipoVehiculo;
        private string matricula;
        private DateTime anio;
        private int kilometraje;
        private List<string> fotos = new List<string>();
        #endregion

        #region Propiedades
        public string Matricula
        {
            get
            {
                return this.matricula;
            }
        }

        public double PrecioPorDia
        {
            get
            {
                return this.tipoVehiculo.PrecioPorDia;
            }
        }

        public string Marca
        {
            get
            {
                return tipoVehiculo.Marca;
            }
        }

        public string Modelo
        {
            get
            {
                return tipoVehiculo.Modelo;
            }
        }

        public DateTime Anio
        {
            get
            {
                return this.anio;
            }
        }

        public List<string> Fotos
        {
            get
            {
                return this.fotos;
            }
        }
        #endregion

        #region Metodos
        public Vehiculo(TipoVehiculo tipo, string matricula, DateTime anio, int kilometraje, List<string> fotos)
        {
            this.tipoVehiculo = tipo;
            this.matricula = matricula;
            this.anio = anio;
            this.kilometraje = kilometraje;
            this.fotos = fotos;
        }

        public override string ToString()
        {
            return String.Format("Matricula: {0}, Marca: {1}, Modelo: {2}, Precio por día: {3}, Kilometraje: {4}, Anio: {5}",
                this.matricula, tipoVehiculo.Marca, tipoVehiculo.Modelo, tipoVehiculo.PrecioPorDia, this.kilometraje, this.anio);
        }
        #endregion
    }
}
