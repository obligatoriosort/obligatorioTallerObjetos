﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Dominio;

namespace WebInterface
{
    public partial class Login : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            
        }

        protected void Login1_Authenticate(object sender, AuthenticateEventArgs e)
        {
            string nombreUsuario = Login1.UserName;
            string pass = Login1.Password;
            bool valido = false;
            string rol = "";
            if (Fachada.ValidarUsuario(nombreUsuario, pass))
            {
                valido = true;
                rol = Fachada.RolUsuario(nombreUsuario);
            }
            if (rol != "" && valido)
            {
                Session["nombreUsuario"] = nombreUsuario;
                Session["rol"] = rol;
                if (Session["rol"].ToString() == "vendedor")
                {
                    Response.Redirect("AltaCliente.aspx");
                }
                else if (Session["rol"].ToString() == "administrador")
                {
                    Response.Redirect("AltaCliente.aspx");
                }

            }
            else
            {
                e.Authenticated = false;
            }
        }
    }
}