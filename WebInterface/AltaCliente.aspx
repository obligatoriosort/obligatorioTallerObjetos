﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Master.Master" AutoEventWireup="true" CodeBehind="AltaCliente.aspx.cs" Inherits="WebInterface.AltaCliente" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script src="scripts/AltaCliente.js"></script>
    <script>console.log(validarParticular());</script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:Button ID="BtnEmpresa" runat="server" Text="Empresa" OnClick="BtnEmpresa_Click" />
    <asp:Button ID="BtnParticular" runat="server" Text="Particular" OnClick="BtnParticular_Click" />
    <asp:Panel ID="PnlEmpresa" runat="server" Visible="False">
        <asp:Label ID="Label2" runat="server" Text="Año de registro"></asp:Label>
        <asp:TextBox ID="txtAnio" runat="server" TextMode="Date" ClientIDMode="Static"></asp:TextBox>
        <asp:RequiredFieldValidator ID="RequiredFieldValidator12" runat="server" ErrorMessage="Ingrese fecha de registro" ControlToValidate="txtAnio">*Error</asp:RequiredFieldValidator>
        <br />
        <asp:Label ID="Label3" runat="server" Text="Telefono"></asp:Label>
        <asp:TextBox ID="txtTel" runat="server" ClientIDMode="Static"></asp:TextBox>
        <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ControlToValidate="txtTel" ErrorMessage="Ingrese Telefono">*Error</asp:RequiredFieldValidator>
        <br />
        <asp:Label ID="Label5" runat="server" Text="RazonSocial"></asp:Label>
        <asp:TextBox ID="txtRazonSocial" runat="server" ClientIDMode="Static"></asp:TextBox>
        <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtRazonSocial" ErrorMessage="Ingrese Razon Social">*Error</asp:RequiredFieldValidator>
        <br />
        <asp:Label ID="Label4" runat="server" Text="Nombre Contacto"></asp:Label>
        <asp:TextBox ID="txtNombreContacto" runat="server" ClientIDMode="Static"></asp:TextBox>
        <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="txtNombreContacto" ErrorMessage="Ingrese Contacto">*Error</asp:RequiredFieldValidator>
        <br />
        <asp:Label ID="Label6" runat="server" Text="Rut"></asp:Label>
        <asp:TextBox ID="txtRut" runat="server" ClientIDMode="Static"></asp:TextBox>
        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtRut" ErrorMessage="Ingrese Rut">*Error</asp:RequiredFieldValidator>
        <asp:ValidationSummary ID="ValidationSummary1" runat="server" />
        <br />
        <br />
        <asp:Button ID="BtnCrearEmpresa" runat="server" OnClientClick="return validarEmpresa()" OnClick="BtnCrearEmpresa_Click" Text="Registrar" />
    </asp:Panel>
    <asp:Panel ID="PnlParticular" runat="server" Visible="False">
        <asp:Label ID="Label1" runat="server" Text="Alta Cliente Particular"></asp:Label>
        <br />
        <asp:Label ID="Label7" runat="server" Text="Año de registro"></asp:Label>
        <asp:TextBox ID="TxtAnioParticular" runat="server" TextMode="Date" ClientIDMode="Static"></asp:TextBox>
        <asp:RequiredFieldValidator ID="RequiredFieldValidator10" runat="server" ControlToValidate="TxtAnioParticular" ErrorMessage="Ingrese fecha">*Error</asp:RequiredFieldValidator>
        <br />
        <asp:Label ID="Label8" runat="server" Text="Telefono"></asp:Label>
        <asp:TextBox ID="TxtTelCliente" runat="server" ClientIDMode="Static"></asp:TextBox>
        <asp:RequiredFieldValidator ID="RequiredFieldValidator9" runat="server" ControlToValidate="TxtTelCliente" ErrorMessage="Ingrese Telefono">*Error</asp:RequiredFieldValidator>
        <br />
        <asp:Label ID="Label" runat="server" Text="Nombre"></asp:Label>
        <asp:TextBox ID="txtNombre" runat="server" ClientIDMode="Static"></asp:TextBox>
        <asp:RequiredFieldValidator ID="RequiredFieldValidator8" runat="server" ControlToValidate="txtNombre" ErrorMessage="Ingrese Nombre">*Error</asp:RequiredFieldValidator>
        <br />
        <asp:Label ID="Label9" runat="server" Text="Apellido"></asp:Label>
        <asp:TextBox ID="txtApellido" runat="server" Height="22px" Width="128px" ClientIDMode="Static"></asp:TextBox>
        <asp:RequiredFieldValidator ID="RequiredFieldValidator7" runat="server" ControlToValidate="txtApellido" ErrorMessage="Ingrese Apellido">*Error</asp:RequiredFieldValidator>
        <br />
        <asp:Label ID="Label10" runat="server" Text="Documento"></asp:Label>
        <asp:TextBox ID="txtDoc" runat="server" ClientIDMode="Static"></asp:TextBox>
        <asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" ControlToValidate="txtDoc" ErrorMessage="Ingrese Documento">*Error</asp:RequiredFieldValidator>
        <br />
        <asp:Label ID="Label11" runat="server" Text="Pais Doc"></asp:Label>
        <asp:TextBox ID="txtPaisDoc" runat="server" ClientIDMode="Static"></asp:TextBox>
        <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ControlToValidate="txtPaisDoc" ErrorMessage="Ingrese pais Documento">*Error</asp:RequiredFieldValidator>
        <br />
        <asp:Label ID="Label12" runat="server" Text="Tipo Doc"></asp:Label>
        <asp:DropDownList ID="ddlTipoDoc" runat="server">
        </asp:DropDownList>
        <asp:RequiredFieldValidator ID="RequiredFieldValidator11" runat="server" ErrorMessage="Seleccione un tipo de Documento" ControlToValidate="ddlTipoDoc">*Error</asp:RequiredFieldValidator>
        <br />
        <br />
        <asp:ValidationSummary ID="ValidationSummary2" runat="server" />
        <br />
        <asp:Button ID="BtnCrearParticular" runat="server" OnClientClick="return validarParticular()" OnClick="BtnCrearParticular_Click" Text="Registrar" />
    </asp:Panel>
        <asp:Label ID="lblError" runat="server"></asp:Label>
        </asp:Content>
