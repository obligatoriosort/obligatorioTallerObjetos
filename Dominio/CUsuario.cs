﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.Serialization;

namespace Dominio
{
    [Serializable]
    public class CUsuario : ISerializable
    {
        #region Atributos

        private List<Usuario> usuarios = new List<Usuario>();
        private static CUsuario instancia;

        #endregion

        #region Propiedades

        public static CUsuario Instancia
        {
            get
            {
                if(instancia == null)
                {
                    instancia = new CUsuario();
                }
                return instancia;
            }
        }

        #endregion

        #region metodos

        /// <summary>
        /// Validación para login de usuario.
        /// </summary>
        /// <param name="nombre"></param>
        /// <param name="password"></param>
        /// <returns>Verdadero si el usuario existe y la passsword es correcta</returns>
        public bool ValidarUsuario(string nombre, string password)
        {
            bool valido = false;
            Usuario user = BuscarUsuario(nombre);

            if(user != null)
            {
                if (user.Password.Equals(password))
                {
                    valido = true;
                }
            }
            return valido;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="nombre"></param>
        /// <returns></returns>
        public string RolUsuario(string nombre)
        {
            string rol = "";
            Usuario user = BuscarUsuario(nombre);

            rol = Enum.GetName(typeof(EnumRol), user.EnumRol);
            //rol = user.EnumRol.ToString();
            return rol;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="rol"></param>
        /// <returns></returns>
        private bool EsEnumRol(string rol)
        {
            bool exito = false;

            string[] turnos = Enum.GetNames(typeof(EnumRol));

            if (turnos.Contains(rol))
            {
                exito = true;
            }
            return exito;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="nombre"></param>
        /// <param name="password"></param>
        /// <param name="rol"></param>
        /// <returns></returns>
        public bool AltaUsuario(string nombre, string password, string rol)
        {
            bool exito = false;

            Usuario user = BuscarUsuario(nombre);

            if(user == null && EsEnumRol(rol) && nombre.Length > 0 && password.Length > 0)
            {
                EnumRol enumRol = (EnumRol)Enum.Parse(typeof(EnumRol), rol);

                usuarios.Add(new Usuario(nombre, password, enumRol));

                exito = true;
            }

            return exito;
        }

        /// <summary>
        /// Busca un usuario por su nombre
        /// </summary>
        /// <param name="nombre"></param>
        /// <returns>Retorna un objeto Usuario o null</returns>
        public Usuario BuscarUsuario(string nombre)
        {
            Usuario user = null;
            int contador = 0;

            while(user == null && contador < usuarios.Count)
            {
                if (usuarios[contador].Nombre.Equals(nombre))
                {
                    user = usuarios[contador];
                }
                contador++;
            }
            return user;
        }
        #endregion

        #region Serialización

        private CUsuario() { }

        private CUsuario(SerializationInfo info, StreamingContext context)
        {
            this.usuarios = info.GetValue("listaUser", typeof(List<Usuario>)) as List<Usuario>;
            CUsuario.instancia = this;
        }
        public void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            info.AddValue("listaUser", this.usuarios, typeof(List<Usuario>));
        }

        #endregion
    }
}
