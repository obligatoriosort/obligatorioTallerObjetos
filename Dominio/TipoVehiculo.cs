﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dominio
{
    [Serializable]
    public class TipoVehiculo
    {
        #region Atributos
        private string marca;
        private string modelo;
        private double precioPorDia;
        #endregion

        #region Propiedades
        public string Marca
        {
            get
            {
                return this.marca;
            }
        }

        public string Modelo
        {
            get
            {
                return this.modelo;
            }
        }

        public double PrecioPorDia
        {
            get
            {
                return this.precioPorDia;
            }
        }
        #endregion

        #region Métodos
        public TipoVehiculo(string marca, string modelo, double precioPorDia)
        {
            this.marca = marca;
            this.modelo = modelo;
            this.precioPorDia = precioPorDia;
        }
        #endregion

    }
}
