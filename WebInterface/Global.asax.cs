﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.SessionState;
using Utility;
using System.IO;
using System.Configuration;
using Dominio;

namespace WebInterface
{
    public class Global : System.Web.HttpApplication
    {

        protected void Application_Start(object sender, EventArgs e)
        {
            // logica de cargado de datos y serialización
            string rutaArchivo =
                HttpRuntime.AppDomainAppPath + ConfigurationManager.AppSettings["rutaArchivo"];

            if (File.Exists(rutaArchivo))
            {
                Intermedia rep = new Intermedia(rutaArchivo);
                rep.DesSerializar();
            }
            else
            {
                this.CargarVehiculos();
                Fachada.CargarDatosPrueba();
            }
        }

        /// <summary>
        /// Cargado inicial de vehiculos y tipos en caso de que no haya una serialización previa
        /// </summary>
        private void CargarVehiculos()
        {
            // hay que cargar usuarios de prueba, y los datos de los vehiculos
            string rutaTipos = HttpRuntime.AppDomainAppPath + ConfigurationManager.AppSettings["rutaTipoVehiculo"];
            string rutaVehiculos = HttpRuntime.AppDomainAppPath + ConfigurationManager.AppSettings["rutaVehiculo"];

            Fachada.CargarTipoVehiculo(rutaTipos);
            Fachada.CargarVehiculo(rutaVehiculos);
        }

        protected void Session_Start(object sender, EventArgs e)
        {

        }

        protected void Application_BeginRequest(object sender, EventArgs e)
        {

        }

        protected void Application_AuthenticateRequest(object sender, EventArgs e)
        {

        }

        protected void Application_Error(object sender, EventArgs e)
        {

        }

        protected void Session_End(object sender, EventArgs e)
        {

        }

        protected void Application_End(object sender, EventArgs e)
        {
            // logica de serializacion
            string rutaArchivo =
            HttpRuntime.AppDomainAppPath + ConfigurationManager.AppSettings["rutaArchivo"];
            Intermedia rep = new Intermedia(rutaArchivo);
            rep.Serializar();
        }
    }
}