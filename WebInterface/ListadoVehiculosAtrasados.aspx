﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Master.Master" AutoEventWireup="true" CodeBehind="ListadoVehiculosAtrasados.aspx.cs" Inherits="WebInterface.ListadoVehiculosAtrasados" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:Label ID="Label1" runat="server" Text="Listar Vehiculos atrasados"></asp:Label>
    <br />
    <asp:Button ID="Button1" runat="server" OnClick="Button1_Click" Text="Generar" />
    <br />
    <br />
    <br />
    <asp:GridView ID="GrillaAlquilerDevuelto" runat="server" AutoGenerateColumns="False">
        <Columns>
            <asp:BoundField AccessibleHeaderText="Costo" DataField="Monto" HeaderText="Costo" />
            <asp:BoundField AccessibleHeaderText="Matricula" DataField="Matricula" HeaderText="Matricula" />
            <asp:BoundField AccessibleHeaderText="Fecha Inicio" DataField="FechaInicio" HeaderText="Fecha Inicio" />
            <asp:BoundField AccessibleHeaderText="FechaFinal" DataField="FechaFinal" HeaderText="FechaFinal" />
            <asp:BoundField AccessibleHeaderText="Datos de Cliente" DataField="Cliente" HeaderText="Datos de Cliente" />
            <asp:BoundField AccessibleHeaderText="Datos de Vehiculo" DataField="Vehiculo" HeaderText="Datos de Vehiculo" />
        </Columns>
</asp:GridView>
</asp:Content>
