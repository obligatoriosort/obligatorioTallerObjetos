﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Dominio;

namespace WebInterface
{
    public partial class DevolucionVehiculo : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["rol"] == null)
            {
                Response.Redirect("Login.aspx");
            }
        }

        protected void btnDevolver_Click(object sender, EventArgs e)
        {
            LblInfo.Visible = false;
            string matricula = txtBoxMatricula.Text;
            Alquiler alq = Fachada.DevolverAlquilado(matricula);
            if (alq != null)
            {
                GridAlquiler.Visible = true;
                btnDevolver.Visible = false;

                List<Alquiler> lista = new List<Alquiler>();
                lista.Add(alq);

                if(lista.Count > 0)
                {
                    GridAlquiler.DataSource = lista;
                    GridAlquiler.DataBind();
                }
                else
                {
                    LblInfo.Visible = true;
                }
                
            }

        }

        
    }
}