﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Dominio;
using System.IO;
using System.Configuration;

namespace WebInterface
{
    public partial class ListadoVehiculosAtrasados : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if(Session["rol"] == null)
            {
                Response.Redirect("Login.aspx");
            }
        }
        protected void Button1_Click(object sender, EventArgs e)
        {
            string ruta = HttpRuntime.AppDomainAppPath + ConfigurationManager.AppSettings["rutaBitacora"];

            GrillaAlquilerDevuelto.Visible = true;
            GrillaAlquilerDevuelto.DataSource = Fachada.ListarAtrasados(Session["nombreUsuario"].ToString(), ruta);
            GrillaAlquilerDevuelto.DataBind();
        }
    }
}