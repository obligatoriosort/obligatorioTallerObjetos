﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Master.Master" AutoEventWireup="true" CodeBehind="CargarVehiculos.aspx.cs" Inherits="WebInterface.CargarVehiculos" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:Label ID="LblCargarTipoVehiculo" runat="server" Text="Cargar tipos de  Vehiculos"></asp:Label><br />
    <asp:Button ID="BtnTipoVehiculo" runat="server" Text="Cargar" OnClick="BtnTipoVehiculo_Click" />
    <asp:Label ID="LblErrorTipoVehiculo" runat="server" Text="Label" Visible="False"></asp:Label>
    <br />
    <hr />
    <asp:Label ID="LblCargarVehiculo" runat="server" Text="Cargar vehiculos"></asp:Label><br />
    <asp:Button ID="BtnCargarVehiculo" runat="server" Text="Cargar" OnClick="BtnCargarVehiculo_Click" />
    <asp:Label ID="LblErrorVehiculo" runat="server" Text="Label" Visible="False"></asp:Label>
</asp:Content>
