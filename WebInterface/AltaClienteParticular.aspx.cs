﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Dominio;

namespace WebInterface
{
    public partial class AltaClienteParticular : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                
                ddlTipoDoc.DataSource = Fachada.TipoDeDocumentosPosibles();
                ddlTipoDoc.DataBind();
            }

        }

       

        protected void btnCrear_Click(object sender, EventArgs e)
        {
            DateTime fechaRegistro = DateTime.MinValue;
            DateTime.TryParse(txtAnio.Text, out fechaRegistro);
            string telefono = txtTel.Text;
            string nombre = txtNombre.Text;
            string apellido = txtApellido.Text;
            string paisdoc = txtPaisDoc.Text;
            string doc = txtDoc.Text;

            if (Fachada.AltaParticular(doc,ddlTipoDoc.SelectedValue, paisdoc, telefono, nombre, apellido,fechaRegistro))
            {
                lblError.Text = "El Evento se dio de alta correctamente";

            }
            else
            {
                lblError.Text = "Verifique los datos";
            }
        }
    }
}