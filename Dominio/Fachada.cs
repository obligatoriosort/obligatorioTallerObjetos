﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dominio
{
    public class Fachada
    {
        #region Atributos
        #endregion

        #region Propiedades
        #endregion

        #region Alquileres
        public static bool AltaAlquiler(string idCliente, string matriculaVehiculo, DateTime fechaInicio, DateTime fechaFinal)
        {
            double costo = 0;
            bool exito = false;

            Vehiculo vehiculo = BuscarVehiculo(matriculaVehiculo);
            Cliente cliente = BuscarCliente(idCliente);

            // Control de fechas habiles
            // if(!(fechaInicio < DateTime.Today){
            //&& CAlquiler.Instancia.BuscarAlquiler(matriculaVehiculo) == null
            if (cliente != null && vehiculo != null && fechaInicio < fechaFinal )
            {
                if(CAlquiler.Instancia.AltaAlquiler(vehiculo, cliente, fechaInicio, fechaFinal)){
                    exito = true;
                }
            }
            // }
            return exito;
        }

        /// <summary>
        /// Cambia el estado de un alquiler como que se devolvio el vehiculo
        /// </summary>
        /// <param name="matricula"></param>
        /// <returns>Verdadero si el alquiler estaba registrado</returns>
        public static Alquiler DevolverAlquilado(string matricula)
        {
            return CAlquiler.Instancia.DevolverVehiculo(matricula);
        }

        public static Alquiler buscarAlquiler(string matricula)
        {
            return CAlquiler.Instancia.BuscarAlquiler(matricula);
        }

        public static List<Alquiler> ListarAtrasados(string usuario, string ruta)
        {
            // Listado de alquileres atrasados
            List<Alquiler> atrasados = new List<Alquiler>();
            atrasados = CAlquiler.Instancia.ListarAtrasados();

            // Generar log de usuario que pide log
            CAlquiler.Instancia.ListaEmisoresAtrasados(usuario, ruta);


            return atrasados;
        }
        #endregion

        #region Usuarios

        public static bool AltaUsuario(string nombre, string password, string rol)
        {
            return CUsuario.Instancia.AltaUsuario(nombre, password, rol);
        }

        public static bool ValidarUsuario(string nombre, string pass)
        {
            bool valido = CUsuario.Instancia.ValidarUsuario(nombre, pass);

            return valido;
        }

        public static string RolUsuario(string nombre)
        {
            return CUsuario.Instancia.RolUsuario(nombre);
        }

        /// <summary>
        /// Cargado de usuarios de prueba
        /// </summary>
        public static void CargarDatosPrueba()
        {
            Fachada.AltaUsuario("vendedor1", "vendedor1", "vendedor");
            Fachada.AltaUsuario("vendedor2", "vendedor2", "vendedor");
            Fachada.AltaUsuario("administrador1", "administrador1", "administrador");
            Fachada.AltaUsuario("administrador2", "administrador2", "administrador");

            Fachada.AltaParticular("49264162", "CI", "Uruguay", "099456575", "Bruce Wayne", "Wayne", new DateTime(2015, 2, 27));
            Fachada.AltaEmpresa("123456789123", "Tel a empresa", "Enzo", "Mejoramos el mundo", new DateTime(2017, 10, 20));

            Fachada.AltaAlquiler("49264162", "SAV8678", new DateTime(2018, 2, 28), new DateTime(2018, 3, 3));
            Fachada.AltaAlquiler("123456789123", "SAV8676", new DateTime(2018, 2, 28), new DateTime(2018, 3, 5));
            Fachada.AltaAlquiler("123456789123", "SAV8685", new DateTime(2018, 2, 28), new DateTime(2018, 3, 15));
        }
        #endregion

        #region Clientes
        public static bool AltaParticular(string id, string tipoDoc, string paisDoc, string telefono, string nombre, string apellido, DateTime anioRegistro)
        {
            bool exito = false;
            Cliente cli = BuscarCliente(id);

            if(cli == null && tipoDoc.Length > 0 && paisDoc.Length > 0 && telefono.Length > 0 && nombre.Length > 0 && apellido.Length > 0)
            {
                exito = CCliente.Instancia.AltaParticular(id, anioRegistro, nombre, telefono, apellido, tipoDoc, paisDoc);
            }

            return exito;
        }

        public static bool AltaEmpresa(string id, string telefono, string nombreContacto, string razonSocial, DateTime anioRegistro)
        {
            bool exito = false;

            Cliente cli = BuscarCliente(id);

            if (cli == null && telefono.Length > 0 && nombreContacto.Length > 0 && razonSocial.Length > 0 && id.Length >= 12)
            {
                exito = CCliente.Instancia.AltaEmpresa(id, anioRegistro, nombreContacto, telefono, razonSocial);
            }
            return exito;
        }
        
        public static Cliente BuscarCliente(string id)
        {
            return CCliente.Instancia.BuscarCliente(id);
        }

        public static List<string> TipoDeDocumentosPosibles()
        {
            return CCliente.Instancia.MostrarTipoDocumentos();
        }
        #endregion

        #region Vehiculos
        public static bool AltaVehiculo (TipoVehiculo tipo, string matricula, DateTime anio, int kilometraje, List<string> fotos)
        {
            bool exito = false;
            Vehiculo veh = BuscarVehiculo(matricula);
            if (veh == null && kilometraje > 0 && matricula.Length > 0  && tipo != null) 
            {
                exito = CVehiculo.Instancia.AltaVehiculo(tipo, matricula, anio, kilometraje, fotos); 
            }
            return exito;

        }

        public static Vehiculo BuscarVehiculo(string matricula)
        {
            return CVehiculo.Instancia.BuscarVehiculo(matricula);
        }

        public static List<Vehiculo> VehiculosDisponibles(DateTime fechaInicio, DateTime fechaFinal)
        {
            List<string> matriculasNoDisponibles = CAlquiler.Instancia.MatriculasNoDisponibles(fechaInicio, fechaFinal);

            List<Vehiculo> vehiculosDisponibles = CVehiculo.Instancia.VehiculosDisponibles(matriculasNoDisponibles);

            return vehiculosDisponibles;
        }

        public static List<string> MarcasDisponibles(DateTime fechaInicio, DateTime fechaFinal)
        {

            List<Vehiculo> vehiculosDisponibles = VehiculosDisponibles(fechaInicio, fechaFinal);
            List<string> marcas = new List<string>();

            foreach(Vehiculo vehiculo in vehiculosDisponibles)
            {
                if (marcas.Contains(vehiculo.Marca))
                {
                    continue;
                }
                marcas.Add(vehiculo.Marca);
            }
            return marcas;
        }

        public static List<string> ModelosDisponibles(DateTime fechaInicio, DateTime fechaFinal, string marca)
        {
            List<Vehiculo> vehiculosDisponibles = VehiculosDisponibles(fechaInicio, fechaFinal);
            List<string> modelos = new List<string>();

            foreach (Vehiculo vehiculo in vehiculosDisponibles)
            {
                if (vehiculo.Marca.Equals(marca))
                {
                    if (modelos.Contains(vehiculo.Modelo))
                    {
                        continue;
                    }
                    modelos.Add(vehiculo.Modelo);
                }
            }

            return modelos;
        }

        public static List<Vehiculo> VehiculosAlquilables(DateTime fechaInicio, DateTime fechaFinal, string marca, string modelo)
        {
            List<Vehiculo> vehiculosDisponibles = VehiculosDisponibles(fechaInicio, fechaFinal);
            List<Vehiculo> vehiculos = new List<Vehiculo>();

            foreach(Vehiculo vehiculo in vehiculosDisponibles)
            {
                if(vehiculo.Marca.Equals(marca) && vehiculo.Modelo.Equals(modelo))
                {
                    vehiculos.Add(vehiculo);
                }
            }

            return vehiculos;
        }


        #region Carga de datos

        public static bool CargarTipoVehiculo(string ruta)
        {
            return CTipoVehiculo.Instancia.CargarTiposVehiculos(ruta);
        }

        public static bool CargarVehiculo(string ruta)
        {
            return CVehiculo.Instancia.CargarVehiculos(ruta);
        }

        #endregion
        #endregion
    }
}
