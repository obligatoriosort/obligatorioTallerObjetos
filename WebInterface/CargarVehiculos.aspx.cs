﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Dominio;
using System.Configuration;
using System.IO;

namespace WebInterface
{
    public partial class CargarVehiculos : System.Web.UI.Page
    {
        private string msgExito = "La carga se ha realizado con exito";

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["rol"] == null)
            {
                Response.Redirect("Login.aspx");
            }
        }


        protected void BtnTipoVehiculo_Click(object sender, EventArgs e)
        {
            string ruta = HttpRuntime.AppDomainAppPath + ConfigurationManager.AppSettings["rutaTipoVehiculo"];

            if (File.Exists(ruta))
            {
                Fachada.CargarVehiculo(ruta);
            }
            LblErrorTipoVehiculo.Text = msgExito;
            LblErrorTipoVehiculo.Visible = true;
        }

        protected void BtnCargarVehiculo_Click(object sender, EventArgs e)
        {
            string ruta = HttpRuntime.AppDomainAppPath + ConfigurationManager.AppSettings["rutaVehiculo"];

            if (File.Exists(ruta))
            {
                Fachada.CargarVehiculo(ruta);
            }
            LblErrorVehiculo.Text = msgExito;
            LblErrorVehiculo.Visible = true;
        }
    }
}