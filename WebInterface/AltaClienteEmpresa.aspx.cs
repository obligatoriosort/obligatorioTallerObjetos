﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Dominio;

namespace WebInterface
{
    public partial class AltaClienteEmpresa : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
          
        }

        protected void TextBox1_TextChanged(object sender, EventArgs e)
        {

        }

        protected void btnCrear_Click(object sender, EventArgs e)
        {
            DateTime fechaRegistro = DateTime.MinValue;
            DateTime.TryParse(txtAnio.Text, out fechaRegistro);
            string telefono = txtTel.Text;
            string nombre = txtNombreContacto.Text;
            string razonSocial = txtRazonSocial.Text;
            string rut = txtRut.Text;
            

            if (Fachada.AltaEmpresa(rut, telefono, nombre,razonSocial,fechaRegistro))
            {
                lblError.Text = "El Evento se dio de alta correctamente";

            }
            else
            {
                lblError.Text = "Verifique los datos";
            }
        }
    }
}