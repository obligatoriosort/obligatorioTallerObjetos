﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.Serialization;
using System.IO;
using System.Globalization;

namespace Dominio
{
    /// <summary>
    /// Controlador de los vehiculos
    /// </summary>
    [Serializable]
    public class CVehiculo : ISerializable
    {
        #region Atributos
        private List<Vehiculo> vehiculos = new List<Vehiculo>();
        private static CVehiculo instancia;
        #endregion

        #region Propiedades
        public static CVehiculo Instancia
        {
            get
            {
                if(instancia == null)
                {
                    instancia = new CVehiculo();
                }
                return instancia;
            }
        }

        public List<Vehiculo> ListaVehiculos
        {
            get
            {
                return this.vehiculos;
            }
        }
        #endregion

        #region Metodos

        public bool CargarVehiculos(string ruta)
        {
            bool exito = false;
            // hay que limpiar esto!

            if (File.Exists(ruta))
            {
                string[] lines = System.IO.File.ReadAllLines(ruta);
                if (lines != null)
                {
                    List<Vehiculo> vehiculos = new List<Vehiculo>();

                    foreach (string line in lines)
                    {
                        List<string> vehiculo = line.Split('@').ToList();
                        // En caso de que la línea este vacía o solo sea un salto de línea
                        if (vehiculo.Count != 6)
                        {
                            continue;
                        }
                        
                        string matricula = vehiculo[0];
                        string marca = vehiculo[1];
                        string modelo = vehiculo[2];
                        DateTime anio = DateTime.Today;
                        DateTime.TryParse(vehiculo[3], out anio);
                        int kilometraje = 0;
                        int.TryParse(vehiculo[4], out kilometraje);

                        List<string> fotos = vehiculo[5].Split('#').ToList();

                        TipoVehiculo tipo = BuscarTipoVehiculo(marca, modelo);

                        if (tipo != null)
                        {
                            AltaVehiculo(tipo, matricula, anio, kilometraje, fotos);
                        }
                    }
                    exito = true;
                }
            }
            return exito;
        }

        /// <summary>
        /// Busca un tipo de vehiculo por marca y por modelo
        /// </summary>
        /// <param name="marca"></param>
        /// <param name="modelo"></param>
        /// <returns>retorna la instancia entera del tipo de vehiculo o null</returns>
        public TipoVehiculo BuscarTipoVehiculo(string marca, string modelo)
        {
            return CTipoVehiculo.Instancia.BuscarTipoVehiculo(marca, modelo);
        }

        /// <summary>
        /// Agrega a List<Vehiculo> vehiculos un nuevo vehiculo.
        /// </summary>
        /// <param name="marca"></param>
        /// <param name="modelo"></param>
        /// <param name="precioPorDia"></param>
        /// <param name="matricula"></param>
        /// <param name="anio"></param>
        /// <param name="disponible"></param>
        /// <param name="kilometraje"></param>
        /// <param name="fotos"></param>
        /// <returns></returns>
        public bool AltaVehiculo(TipoVehiculo tipo, string matricula, DateTime anio, int kilometraje, List<string> fotos)
        {
            bool exito = false;

            Vehiculo vehiculo = BuscarVehiculo(matricula);

            if (vehiculo == null)
            {
                vehiculos.Add(new Vehiculo(tipo, matricula, anio, kilometraje, fotos));
                exito = true;
            }
            return exito;
        }

        /// <summary>
        /// Busca el vehiculo según el argumento matricula
        /// </summary>
        /// <param name="matricula">Matricula del vehiculo que se busca</param>
        /// <returns>Vehiculo encontrado, o null</returns>
        public Vehiculo BuscarVehiculo(string matricula)
        {
            Vehiculo vehiculo = null;
            int contador = 0;

            while(vehiculo == null && contador < vehiculos.Count)
            {
                if (vehiculos[contador].Matricula.Equals(matricula))
                {
                    vehiculo = vehiculos[contador];
                }
                contador++;
            }
            return vehiculo;
        }

        /// <summary>
        /// Compara la lista de vehiculos registrados con una lista de matriculas, y busca vehiculos con matriculas diferentes
        /// </summary>
        /// <param name="matriculas">Lista de matriculas para comparar</param>
        /// <returns>Retorna una lista de vehiculos</returns>
        public List<Vehiculo> VehiculosDisponibles(List<string> matriculas)
        {
            List<Vehiculo> autosDisponibles = new List<Vehiculo>();

            foreach(Vehiculo auto in vehiculos)
            {
                if (!matriculas.Contains(auto.Matricula))
                {
                    autosDisponibles.Add(auto);
                }
            }
            return autosDisponibles;
        }

        #region Serialización

        private CVehiculo() { }

        private CVehiculo(SerializationInfo info, StreamingContext context)
        {
            this.vehiculos = info.GetValue("listaVehiculos", typeof(List<Vehiculo>)) as List<Vehiculo>;
            CVehiculo.instancia = this;
        }
        public void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            info.AddValue("listaVehiculos", this.vehiculos, typeof(List<Vehiculo>));
        }

        #endregion

        #endregion
    }
}
