﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Master.Master" AutoEventWireup="true" CodeBehind="AltaAlquiler.aspx.cs" Inherits="WebInterface.AltaAlquiler" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script src="scripts/AltaAlquiler.js"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:Panel ID="PnlCliente" runat="server">
        <asp:Label ID="LblIdCliente" runat="server" Text="Identificador Cliente"></asp:Label>
        <asp:TextBox ID="TextBoxIdCliente" runat="server" ClientIDMode="Static"></asp:TextBox>
        <asp:RequiredFieldValidator ID="RequiredIdCliente" runat="server" ControlToValidate="TextBoxIdCliente" ErrorMessage="*">Identificador requerido</asp:RequiredFieldValidator>
        <asp:Button ID="BtnBuscCliente" ClientIDMode="Static" OnClientClick="return validarClienteVacio()" runat="server" BorderColor="#FF66FF" BorderStyle="Solid" ForeColor="#003399" Height="25px" OnClick="BtnBuscCliente_Click" Text="Buscar" Width="117px" />
    </asp:Panel>
    <asp:Panel ID="PnlFecha" runat="server" Visible="False">
        <asp:Label ID="LblFecha" runat="server" Text="Fecha de alquiler"></asp:Label><br />
        <!-- Hora inicio -->
        <asp:Label ID="LblFechaInicio" runat="server" Text="Fecha de Inicio"></asp:Label>
        <asp:TextBox ID="TxtBoxFechaInicio" runat="server" TextMode="Date"></asp:TextBox>
        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="TxtBoxFechaInicio" ErrorMessage="Fecha incorrecta">*Error</asp:RequiredFieldValidator>
        <asp:TextBox ID="TxtBoxHoraInicio" runat="server" TextMode="Time"></asp:TextBox>
        <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="TxtBoxHoraInicio" ErrorMessage="Hora incorrecta">*Error</asp:RequiredFieldValidator><br />
        <!-- Hora entrega -->
        <asp:Label ID="LblFechaEntrega" runat="server" Text="Fecha de Entrega"></asp:Label>
        <asp:TextBox ID="TxtBoxFechaEntrega" ClientIDMode="Static" runat="server" TextMode="Date"></asp:TextBox>
        <asp:RequiredFieldValidator ID="ReqFieldFecha" runat="server" ControlToValidate="TxtBoxFechaEntrega" ErrorMessage="Fecha incorrecta">*Error</asp:RequiredFieldValidator>
        <asp:TextBox ID="TxtBoxHoraEntrega" ClientIDMode="Static" runat="server" TextMode="Time"></asp:TextBox>
        <asp:RequiredFieldValidator ID="RequiredFechaEntrega" runat="server" ControlToValidate="TxtBoxHoraEntrega" ErrorMessage="Hora incorrecta">*Error</asp:RequiredFieldValidator><br />
        <!-- Buscar alquiler -->
        <asp:Button ID="BtnBuscarVehiculo" ClientIDMode="Static" runat="server" OnClick="BtnBuscarVehiculo_Click" Text="Buscar vehiculo" />
        <asp:Label ID="LblErrorFecha" runat="server" Text="Las fechas no son validas!" Visible="False"></asp:Label>
        <br />
        
    </asp:Panel>
    <asp:Panel ID="PnlMarca" runat="server" Visible="False">
        <asp:ListBox ID="ListBoxMarcas" ClientIDMode="Static" AutoPostBack="true" runat="server" OnSelectedIndexChanged="ListBoxMarcas_SelectedIndexChanged"></asp:ListBox>
    </asp:Panel>
    <asp:Panel ID="PnlModelo" runat="server" Visible="False">
        <asp:ListBox ID="ListBoxModelos" ClientIDMode="Static" AutoPostBack="true" runat="server" OnSelectedIndexChanged="ListBoxModelos_SelectedIndexChanged"></asp:ListBox>
    </asp:Panel>
    <asp:Panel ID="PnlVehiculo" runat="server" Visible="False">
        <asp:Repeater ID="Repeater1" runat="server" OnItemDataBound="Repeater2_ItemDataBound">
            <HeaderTemplate>
                <table>
                <tr>
                    <th>Alquilar</th>
                </tr>
            </HeaderTemplate>

            <ItemTemplate>
                <tr>
                    <td>
                        <asp:HiddenField ID="HfMatricula" Value='<%# Eval("Matricula")%>' runat="server" />
                        <asp:Button ID="chkBox" runat="server" text="Alquilar" OnClick="BtnAlquilar" /> 
                    </td>
                    <td>
                        <p><%# Eval("Matricula") %></p>
                    </td>   
                    <asp:Repeater ID="Repeater2" runat="server">
                        <ItemTemplate>
                            <td>
                                <asp:Image ID="Image1" height="32" width="32" runat="server" ImageUrl='<%# Container.DataItem.ToString() %>' />
                            </td>
                        </ItemTemplate>
                    </asp:Repeater>
                </tr>
            </ItemTemplate>

            <FooterTemplate>
                </table>
            </FooterTemplate>
        </asp:Repeater>
    </asp:Panel>
    <asp:Panel ID="PnlAlquilado" runat="server" Visible="False">
        <asp:GridView ID="GrillaAlquilados" ClientIDMode="Static" runat="server" AutoGenerateColumns="False">
            <Columns>
                <asp:BoundField AccessibleHeaderText="Matricula" DataField="Matricula" HeaderText="Matricula" />
                <asp:BoundField AccessibleHeaderText="Costo total" DataField="Monto" HeaderText="Costo total" />
                <asp:BoundField AccessibleHeaderText="Fecha Inicio" DataField="FechaInicio" HeaderText="Fecha Inicio" />
                <asp:BoundField AccessibleHeaderText="Fecha Final" DataField="FechaFinal" HeaderText="Fecha Final" />
            </Columns>
        </asp:GridView>
        <asp:Label ID="LblAlquilado" runat="server" Text="Alquiler exitoso" Visible="false"></asp:Label>
    </asp:Panel>
</asp:Content>
